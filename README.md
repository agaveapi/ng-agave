# Angular2 Agave API SDK

Angular2 SDK for the Agave API

## Install

```
npm install ng-agave --save
```
## Configure clients

You must set your own `baseURI` and `oAuthAccessToken` in `node_modules/ng-agave/src/http/http.configuration.ts` for authentication:

```
  constructor(){
    this.baseURI = 'https://public.agaveapi.co';
    this.oAuthAccessToken = '9af08...';
  }
```

An easy way to register your client and obtain your `oAuthAccessToken` is through the CLI (https://bitbucket.org/agaveapi/cli):
```
$ tenants-init
Please select a tenant from the following list:
[0] agave.prod
[1] araport.org
[2] designsafe
[3] iplantc.org
[4] irec
[5] irmacs
[6] sgci
[7] tacc.prod
[8] vdjserver.org
Your choice [3]: 0
You are now configured to interact with the APIs at https://public.agaveapi.co/

$ clients-create -N "agave_client" -S
API username : yourusername
API password: yourpassword
Successfully created client agave_client
key: 2YR...
secret: NaH3...
$ auth-check -v
{
  "tenantid": "agave.prod",
  "baseurl": "https://public.agaveapi.co",
  "devurl": "",
  "apisecret": "NaH3...",
  "apikey": "2YR...",
  "username": "yourusername",
  "access_token": "9af08...",
  "refresh_token": "",
  "created_at": "",
  "expires_in": "",
  "expires_at": ""
}
```
## Usage example

```TypeScript
// my.module.ts
import {NgModule} from '@angular/core';
import { HttpModule } from '@angular/http'; <-- import Angular HTTP module
import {BrowserModule} from '@angular/platform-browser';
import { HttpClient } from 'ng-agave/ng-agave'; // <-- import API Client
import { Configuration } from 'ng-agave/ng-agave'; // <-- import API Config
import { APIHelper } from 'ng-agave/ng-agave'; // <-- import API Helper
import {MyComponent} from './my.component';

@NgModule({
    imports: [BrowserModule, HttpModule],
    declarations: [MyComponent],
    providers: [HttpClient, Configuration, APIHelper], // <-- include them once
    bootstrap: [MyComponent]
})
export class MyAppModule {}
```

```TypeScript
// my.component.ts
import {Component} from '@angular/core';
import { AppsService } from 'ng-agave/ng-agave'; // <-- import service wanted
@Component({
    selector: 'my-component',
    templateUrl: './my-component.html',
    providers: [
          AppsService, // <-- include service
    ]
})
export class MyComponent {
     constructor(private appsService:AppsService){}
     ngOnInit(){
        this.appsService.searchApps('')
            .subscribe(
                data => {console.log(data)},
                err => console.log(err)
            );
     }
}
```

## Build

Requires globally-installed `node` & `npm`.

```
cd ng-agave
npm install
npm run prepublish #This will generates the .js, .d.ts and .js.map files
```

## Test your own build locally

```
cd ng-agave
npm run prepublish
npm link

cd path/to/your/angular2project
npm link ng-agave #This will install the library in your project as if you had installed it with npm install
```
## Test (optional)
```
ng test
```
