import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Request } from '@angular/http';
import { Response } from '@angular/http';
import { APIHelper } from './http.apihelper';
import { HttpResponse } from './http.response';
import { HttpContext } from './http.context';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'


@Injectable()
export class HttpClient{

  private APIHelper: APIHelper;

  constructor (private http: Http) {};

  convertHttpRequest(req: any): Request{
    let options: any = {
      url: req.queryUrl,
      method: req.method,
      headers: req.headers,
      cache: req.cache,
    }

    if (req.username) {
        options.headers = options.headers || {};
        options.headers["Authorization"] = "Basic " + this.APIHelper.base64Encode(req.username + ":" + req.password);
    }
    if (req.body) {
        options.body= req.body;
    }
    if (req.formData) {
        options.data = this.APIHelper.createFormData(req.formData);
        options.headers["content-type"] = undefined;
    }
    if (req.form) {
        options.data = req.form;
        options.transformRequest = (obj: Object): Object => {
          let encoded: Object = this.APIHelper.urlEncodeObject(obj);
          return encoded;
        };
        options.headers["content-type"] = 'application/x-www-form-urlencoded';
    }
    return options;
  }

  convertHttpResponse(resp: any) {
    let response = new HttpResponse();
    if (resp) {
        response.body = resp.data;
        response.headers = resp.headers;
        response.statusCode = resp.status;
    }

    return response;
  };

  executeRequest(req: Object): Observable<any> {
      let convertedRequest = this.convertHttpRequest(req);
      let context = new HttpContext();
      let response = this.http.request(convertedRequest.url, convertedRequest);

      return response.map(response => <any> response.json());
  }
}
