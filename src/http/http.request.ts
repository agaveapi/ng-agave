export interface HttpRequest {
  queryUrl: string;
  method: string;
  headers: string;
  username: string;
  password: string;
  body: string;
  formData: string;
  form: string;
}
