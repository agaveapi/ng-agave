import { Injectable } from '@angular/core';

@Injectable()
export class Configuration{

  private baseURI: string;
  private oAuthAccessToken: string;

  constructor(){
    this.baseURI = '';
    this.oAuthAccessToken = '';
  }

  getBaseURI(){
    return this.baseURI;
  }

  getOAuthAccessToken(){
    return this.oAuthAccessToken;
  }


}
