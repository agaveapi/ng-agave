import { Injectable } from '@angular/core';
import { HttpContext } from './http.context';

@Injectable()
export class APIHelper{

  private _keyStr: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

  private _utf8_encode( string: string ) : string {
    string = string.replace(/\r\n/g, "\n");
    let utftext = "";

    for (var n = 0; n < string.length; n++) {
      var c = string.charCodeAt(n);

      if (c < 128) {
          utftext += String.fromCharCode(c);
      } else if ((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      } else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }
    }
    return utftext;
  }

  private _utf8_decode( utftext: string ) : string {
    let string = "";
    let i = 0;
    let c = 0, c1 = 0, c2 = 0, c3 = 0;

    while (i < utftext.length) {
      c = utftext.charCodeAt(i);
      if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      } else if ((c > 191) && (c < 224)) {
        c2 = utftext.charCodeAt(i + 1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      } else {
        c2 = utftext.charCodeAt(i + 1);
        c3 = utftext.charCodeAt(i + 2);
        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }
    }
    return string;
  }

  appendUrlWithTemplateParameters( queryBuilder: string, parameters: Object) : string {
    if (queryBuilder == null) {
      return;
    }

    if (parameters == null) {
      return queryBuilder;
    }

    for (var key in parameters) {
      let replaceValue = "";
      let element = (<any>parameters)[key];

      if (element == null) {
          replaceValue = "";
      } else if (element instanceof Array) {
          replaceValue = element.join("/");
      } else {
          replaceValue = element.toString();
      }
      queryBuilder = queryBuilder.replace('{' + (key) + '}', replaceValue)
    }
    return queryBuilder;
  }


  public appendUrlWithQueryParameters( queryBuilder: string, parameters: Object) : string {
    if (queryBuilder == null) {
      return;
    }
    if (parameters == queryBuilder) {
      return queryBuilder;
    }

    var hasParams = queryBuilder.indexOf('?') > -1;
    var encoded = this.urlEncodeObject(parameters);
    var separator = (hasParams) ? '&' : '?'
    queryBuilder = queryBuilder + separator + encoded;
    return queryBuilder;
  }

  cleanUrl( url: string ): string{
    let re = /^https?:\/\/[^\/]+/;
    let str:string = url;

    var match = url.match(re);
    if (match == null) {
      return;
    }

    var protocol = match[0];
    var queryUrl = url.substring(protocol.length);
    queryUrl = queryUrl
                .replace(/\/\/+/, '/')
                .replace(/\/+/g, '/');

    var result = protocol + queryUrl;
    return result;
  }

  base64Encode( input:string ): string {
    if (typeof btoa !== 'undefined') {
        return btoa(input);
    }
    let output = '';
    let chr1: number, chr2: number, chr3: number, enc1: number, enc2: number, enc3: number, enc4: number;
    let i = 0;

    input = this._utf8_encode(input);

    while (i < input.length) {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }

      output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
    }

    return output;
  }

  base64Decode(input: string) {
    if (typeof atob !== "undefined") {
      return atob(input);
    }
    let e = {};
    let i: number, b: number = 0;
    let c: number, x: number , l: number = 0;
    let a: number, r: string = '';
    let w = String.fromCharCode;
    let L: number = input.length;
    var A = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    for (i = 0; i < 64; i++) {
      (<any>e)[A.charAt(i)] = i;
    }
    for (x = 0; x < L; x++) {
      c = (<any>e)[input.charAt(x)];
      b = (b << 6) + c;
      l += 6;
      while (l >= 8) {
          ((a = (b >>> (l -= 8)) & 0xff) || (x < (L - 2))) && (r += w(a));
      }
    }
    return r;
  }

  jsonSerialize( data: Object ) {
    return JSON.stringify(data);
  }

  formatString( str: string, name: string, i: number): string {
    if (!str || arguments.length <= 1) return str;
    let args = arguments;
    for (let i = 1; i < arguments.length; i++) {
      let reg = new RegExp("\\{" + (i - 1) + "\\}", "gm");
      str = str.replace(reg, arguments[i]);
    }
    return str;
  }

  cleanObject( input: any ): Object{
    let key: any;
    for (key in input) {
      let value: any = input[key];
        if (typeof value === 'undefined' || value === null) {
          if (input.constructor === Array) {
              input.splice(key, 1)
          }
          else delete input[key];
        } else if (Object.prototype.toString.call(value) === '[object Object]') {
          this.cleanObject(value);
        } else if (value.constructor === Array) {
          this.cleanObject(value);
        }
    };
    return input;
  }

  createFormData( obj: Object ): Object {
    this.cleanObject(obj);
    let formData = new FormData();
    let dictionary = this.formDataEncodeObject(obj, undefined);
    for (let key in dictionary) {
        formData.append(key, (<any>dictionary)[key]);
    }
    return formData;
  }

  prepareFormFieldsFromArray( name:string, values:Array<string>): any {
    let element: any = null;
    let formFields: any;

    if (!values) return formFields;

    for (let i = 0; i < values.length; i++) {
      //replace null values with empty string to maintain index order
      let elemValue = values[i] || "";
      let key:string = this.formatString("{0}[{1}]", name, i);
      formFields[key] = elemValue;
    }
    return formFields;
  }

  merge(first: Object, second: Object) {
    for (let attrname in second) {
        (<any>first)[attrname] = (<any>second)[attrname];
    }
    return first;
  }

  appendContext(item: any, context: HttpContext): HttpContext{
    if (!(item instanceof Object)) return;

    item.getContext = (context: HttpContext): HttpContext => {
       return context;
    }

    return item;
  }

  formDataEncodeObject( obj: Object, keys: Object ): Object {
    let query: string = '';
    let name: string;
    let value: Object;
    let fullSubName: string;

    let subName: string;
    let subValue: string;
    let innerObj: Object;
    let i: number;

    if (!keys) {
        keys = {};
    }
    for (name in obj) {
      value = (<any>obj)[name];
      if (value instanceof File) {
        (<any>keys)[name] = value;
      } else if (value instanceof Array) {
        for (i = 0; i < value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          (<any>innerObj)[fullSubName] = subValue;
          this.formDataEncodeObject(innerObj, keys);
        }
      }
      else if (value instanceof Object) {
        for (subName in value) {
          subValue = (<any>value)[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          (<any>innerObj)[fullSubName] = subValue;
          this.formDataEncodeObject(innerObj, keys);
        }
      }
      else if (value !== undefined && value !== null) {
        if (!(value instanceof Object)) {
        (<any>keys)[name] = value;
        }
      }
    }
    return keys;
  }

  public urlEncodeObject( obj: Object ): Object {
    let dict: Object = this.formDataEncodeObject(obj, undefined);
    let query = "";
    for (var name in dict) {
      let value = (<any>dict)[name];
      query = query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&'
    }
    return query.length ? query.substr(0, query.length - 1) : query;
  }

}
