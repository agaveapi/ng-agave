import { Injectable } from '@angular/core';
import { APIHelper } from '../http/http.apihelper';
import { Configuration } from '../http/http.configuration';
import { HttpClient } from '../http/http.client';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ClientsService{

  constructor(private HttpClient:HttpClient, private Configuration:Configuration, private APIHelper:APIHelper){};

  listClients():Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/clients/v2/";

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  addClient(body:Object):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/clients/v2/";

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  getClient(clientName:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/clients/v2/{clientName}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "clientName": clientName
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  deleteClient(clientName:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/clients/v2/{clientName}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "clientName": clientName
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  addClientAPISubscription(body:Object, clientName:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/clients/v2/{clientName}/subscriptions";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "clientName": clientName
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  deleteClearClientAPISubscriptions(clientName:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/clients/v2/{clientName}/subscriptions";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "clientName": clientName
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  listClientApiSubscriptions(clientName:string, limit:number, offset:number):Observable<any> {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/clients/v2/{clientName}/subscriptions";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "clientName": clientName
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }
}
