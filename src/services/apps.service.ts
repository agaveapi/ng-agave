import { Injectable } from '@angular/core';
import { APIHelper } from '../http/http.apihelper';
import { Configuration } from '../http/http.configuration';
import { HttpClient } from '../http/http.client';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class AppsService{

  constructor(private HttpClient:HttpClient, private Configuration:Configuration, private APIHelper:APIHelper){};

  searchApps(query:string){
    let baseUri = this.Configuration.getBaseURI();;
    let queryBuilder = query ? baseUri + "/apps/v2/?" + query : baseUri + "/apps/v2/";

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
        cache: false
    };

    return this.HttpClient.executeRequest(config);
  }

  listApps(limit:number, offset:number, queryParameters:string) {
    limit = limit || 100;
    offset = offset || 0;
    queryParameters = queryParameters || null;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/apps/v2/";

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, queryParameters)

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
        cache: true
    };

    return this.HttpClient.executeRequest(config);
  }

  addApp(body:Object) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/";

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "content-type": "application/json; charset=utf-8",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      this.APIHelper.cleanObject(body);

      let config = {
          method: "POST",
          queryUrl: queryUrl,
          headers: headers,
          body: body
      };
      return this.HttpClient.executeRequest(config);
  }

  getAppDetails(appId:string) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "GET",
          queryUrl: queryUrl,
          headers: headers,
      };
      return this.HttpClient.executeRequest(config);
  }

  updateApp(appId:string, body:Object) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "content-type": "application/json; charset=utf-8",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      this.APIHelper.cleanObject(body);

      let config = {
          method: "POST",
          queryUrl: queryUrl,
          headers: headers,
          body: body
      };
      return this.HttpClient.executeRequest(config);
  }

  updateInvokeAppAction(appId:string, body:Object) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "content-type": "application/json; charset=utf-8",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      this.APIHelper.cleanObject(body);

      let config = {
          method: "PUT",
          queryUrl: queryUrl,
          headers: headers,
          body: body
      };
      return this.HttpClient.executeRequest(config);
  }

  deleteApp(appId:string) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "DELETE",
          queryUrl: queryUrl,
          headers: headers,
      };
      return this.HttpClient.executeRequest(config);
  }

  addAppPermission(appId:string, body:Object) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}/pems";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "content-type": "application/json; charset=utf-8",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      this.APIHelper.cleanObject(body);

      let config = {
          method: "POST",
          queryUrl: queryUrl,
          headers: headers,
          body: body
      };
      return this.HttpClient.executeRequest(config);
  }

  deleteClearAppPermissions(appId:string) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}/pems";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "DELETE",
          queryUrl: queryUrl,
          headers: headers,
      };
      return this.HttpClient.executeRequest(config);
  }

  getAppPermission(appId:string, username:string) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}/pems/{username}";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId,
          "username": username
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "GET",
          queryUrl: queryUrl,
          headers: headers,
      };
      return this.HttpClient.executeRequest(config);
  }

  updateAppPermission(appId:string, body:Object, username:string) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}/pems/{username}";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId,
          "username": username
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "content-type": "application/json; charset=utf-8",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      this.APIHelper.cleanObject(body);

      let config = {
          method: "POST",
          queryUrl: queryUrl,
          headers: headers,
          body: body
      };
      return this.HttpClient.executeRequest(config);
  }

  deleteAppPermission(appId:string, username:string) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}/pems/{username}";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId,
          "username": username
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "DELETE",
          queryUrl: queryUrl,
          headers: headers,
      };
      return this.HttpClient.executeRequest(config);
  }

  getAppSubmissionForm(appId:string) {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}/form";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "GET",
          queryUrl: queryUrl,
          headers: headers,
      };

  }

  listAppPermissions(appId:string, limit:number, offset:number) {
      limit = limit || 100;
      offset = offset || 0;

      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}/pems";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId
      });

      queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
          "limit": (null != limit) ? limit : 100,
          "offset": (null != offset) ? offset : 0
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "GET",
          queryUrl: queryUrl,
          headers: headers,
      };
      return this.HttpClient.executeRequest(config);
  }

  listAppHistory(appId:string, created:string, limit:number, offset:number, status:string) {
      limit = limit || 100;
      offset = offset || 0;

      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/apps/v2/{appId}/history";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "appId": appId
      });

      queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
          "created": created,
          "limit": (null != limit) ? limit : 100,
          "offset": (null != offset) ? offset : 0,
          "status": status
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "GET",
          queryUrl: queryUrl,
          headers: headers,
      };
      return this.HttpClient.executeRequest(config);
  }

}
