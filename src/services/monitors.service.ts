import { Injectable } from '@angular/core';
import { APIHelper } from '../http/http.apihelper';
import { Configuration } from '../http/http.configuration';
import { HttpClient } from '../http/http.client';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class MonitorsService{

  constructor(private HttpClient:HttpClient, private Configuration:Configuration, private APIHelper:APIHelper){};

  addMonitoringTasks(body:Object):Observable<any> {

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/monitors/v2/";

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  getMonitoringTask(monitorId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/monitors/v2/{monitorId}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "monitorId": monitorId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  updateMonitoringTask(body:Object, monitorId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/monitors/v2/{monitorId}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "monitorId": monitorId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  deleteMonitoringTask(monitorId:String):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/monitors/v2/{monitorId}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "monitorId": monitorId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  listMonitoringTaskChecks(monitorId:string, endDate:string, limit:number, offset:number, result:string, startDate:string):Observable<any> {
    limit = limit || 250;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/monitors/v2/{monitorId}/checks";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "monitorId": monitorId
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "naked": true,
        "endDate": endDate,
        "limit": (null != limit) ? limit : 250,
        "offset": (null != offset) ? offset : 0,
        "result": (result != null) ? result : null,
        "startDate": startDate
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  searchMonitoringTaskChecks(monitorId:string, query:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = query ? baseUri + "/monitors/v2/{monitorId}/checks?" + query : baseUri + "/monitors/v2/{monitorId}/checks";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "monitorId": monitorId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
        cache: false
    };
    return this.HttpClient.executeRequest(config);
  }

  createForceMonitoringTaskCheck(monitorId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/monitors/v2/{monitorId}/checks";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "monitorId": monitorId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  getMonitoringTaskCheck(checkId:string, monitorId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/monitors/v2/{monitorId}/checks/{checkId}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "checkId": checkId,
        "monitorId": monitorId
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "naked": true
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  listMonitoringTasks(active:string, limit:number, offset:number, target:string):Observable<any> {

    active = active || "true";
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/monitors/v2/";

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "naked": true,
        "active": (null != active) ? active : "true",
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0,
        "target": target
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  searchMonitors(query:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = query ? baseUri + "/monitors/v2/?" + query : baseUri + "/monitors/v2/";

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

}
