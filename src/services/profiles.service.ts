import { Injectable } from '@angular/core';
import { APIHelper } from '../http/http.apihelper';
import { Configuration } from '../http/http.configuration';
import { HttpClient } from '../http/http.client';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ProfilesService{

  constructor(private HttpClient:HttpClient, private Configuration:Configuration, private APIHelper:APIHelper){};

  getProfile(username:string):Observable<any> {
    username = username || "me";

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/profiles/v2/{username}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "username": username
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  deleteClearInternalUsers(apiUsername:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/profiles/v2/{apiUsername}/users";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "apiUsername": apiUsername
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  addInternalUser(apiUsername:string, body:Object):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/profiles/v2/{apiUsername}/users";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "apiUsername": apiUsername
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "naked": true
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  updateInternalUser(apiUsername:string, body:Object, internalUsername:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/profiles/v2/{apiUsername}/users/{internalUsername}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "apiUsername": apiUsername,
        "internalUsername": internalUsername
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  deleteInternalUser(apiUsername:string, internalUsername:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/profiles/v2/{apiUsername}/users/{internalUsername}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "apiUsername": apiUsername,
        "internalUsername": internalUsername
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "naked": true
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  listProfiles(email:string, name:string, username:string, first_name:string, last_name:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/profiles/v2/";

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
      "naked": true,
      "email": email,
      "name": name,
      "first_name": first_name,
      "last_name": last_name,
      "username": username
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
      "accept": "application/json",
      "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
      method: "GET",
      queryUrl: queryUrl,
      headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  listInternalUsers(apiUsername:string, email:string, name:string, username:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/profiles/v2/{apiUsername}/users";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "apiUsername": apiUsername
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "email": email,
        "name": name,
        "username": username
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  getInternalUser(apiUsername:string, internalUsername:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/profiles/v2/{apiUsername}/users/{internalUsername}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "apiUsername": apiUsername,
        "internalUsername": internalUsername
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  addProfile(body:Object):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/profiles/v2";

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "naked": true
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  updateProfile(apiUsername:string, body:Object):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/profiles/v2/{apiUsername}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "apiUsername": apiUsername
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }
}
