import { Injectable } from '@angular/core';
import { APIHelper } from '../http/http.apihelper';
import { Configuration } from '../http/http.configuration';
import { HttpClient } from '../http/http.client';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class SystemsService{

  constructor(private HttpClient:HttpClient, private Configuration:Configuration, private APIHelper:APIHelper){};

  addSystem(body:Object):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/";

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  addSystemCredential (body: Object, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/credentials";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  addSystemRole(body:Object, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/{systemId}/roles";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteSystem (systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
      "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
      "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteClearSystemRoles(systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/roles";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteClearSystemAuthCredentials(systemId:string):Observable<any> {
    let baseUri =  this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/credentials";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteClearSystemAuthCredentialsForInternalUser(internalUsername:string, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/credentials/{internalUsername}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "internalUsername": internalUsername,
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteSystemAuthCredentialForInternalUser(credentialType:string, internalUsername:string, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/credentials/{internalUsername}/{credentialType}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "credentialType": (credentialType != null) ? credentialType : null,
        "internalUsername": internalUsername,
        "systemId": systemId
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "naked": true
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteSystemRole(systemId:string, username:string):Observable<any> {

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/roles/{username}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId,
        "username": username
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  getSystemDetails(systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  getSystemCredential(credentialType:string, internalUsername:string, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/credentials/{internalUsername}/{credentialType}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "credentialType": (credentialType != null) ? credentialType : null,
        "internalUsername": internalUsername,
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  getSystemRole(systemId:string, username:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/roles/{username}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId,
        "username": username
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  listSystemCredentials(systemId:string, limit:number, offset:number):Observable<any> {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/credentials";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  listCredentialsForInternalUser (internalUsername:string, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/credentials/{internalUsername}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "internalUsername": internalUsername,
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  listSystems(limit:number, offset:number, defaultSystems:boolean, publicSystems:boolean, type:string):Observable<any> {
    offset = offset || 0;
    limit = limit || 9999999;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/";

    let params = {
        "offset": offset,
        "limit": (null != limit) ? limit : 100,
        "type": (type != null) ? type : null
    };

    if (defaultSystems) (<any>params)["default"] = defaultSystems;
    if (publicSystems) (<any>params)["public"] = publicSystems;
    if (type) (<any>params)["type"] = type;

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, params);

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
        cache: true
    };

    return this.HttpClient.executeRequest(config);
  }

  listSystemRoles(systemId:string, limit:number, offset:number):Observable<any> {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/roles";
    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  searchSystems(query:string):Observable<any>{
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = query ? baseUri + "/systems/v2/?" + query : baseUri + "/systems/v2/";
    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: 'GET',
        queryUrl: queryUrl,
        headers: headers,
        cache: true
    };

    return this.HttpClient.executeRequest(config);
  }

  updateCloneSystem(body:Object, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "naked": true
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "PUT",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  updateInvokeSystemAction(body:Object, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "PUT",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  updateSystem(body:Object, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  updateSystemCredential(body:Object, internalUsername:string, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/credentials/{internalUsername}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "internalUsername": internalUsername,
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  updateSystemCredentialOfType(body:Object, credentialType:string, internalUsername:string, systemId:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/credentials/{internalUsername}/{credentialType}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "credentialType": (credentialType != null) ? credentialType : null,
        "internalUsername": internalUsername,
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  updateSystemRole (body:Object, systemId:string, username:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/systems/v2/{systemId}/roles/{username}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId,
        "username": username
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

}
