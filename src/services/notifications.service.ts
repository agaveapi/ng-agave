import { Injectable } from '@angular/core';
import { APIHelper } from '../http/http.apihelper';
import { Configuration } from '../http/http.configuration';
import { HttpClient } from '../http/http.client';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class NotificationsService{

  constructor(private HttpClient:HttpClient, private Configuration:Configuration, private APIHelper:APIHelper){};

    searchNotifications(query:string):Observable<any> {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = query ? baseUri + "/notifications/v2/?" + query : baseUri + "/notifications/v2/";

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "GET",
          queryUrl: queryUrl,
          headers: headers,
          cache: false
      };
      return this.HttpClient.executeRequest(config);
    }

    addNotification(body:Object):Observable<any> {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/notifications/v2/";

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "content-type": "application/json; charset=utf-8",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      this.APIHelper.cleanObject(body);

      let config = {
          method: "POST",
          queryUrl: queryUrl,
          headers: headers,
          body: body
      };
      return this.HttpClient.executeRequest(config);
    }

    listNotifications(associatedUuid:string, limit:number, offset:number):Observable<any> {
      limit = limit || 100;
      offset = offset || 0;

      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/notifications/v2/";

      queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
          "associatedUuid": associatedUuid,
          "limit": (null != limit) ? limit : 100,
          "offset": (null != offset) ? offset : 0
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "GET",
          queryUrl: queryUrl,
          headers: headers,
          cache: false
      };
      return this.HttpClient.executeRequest(config);
    }

    getNotification(uuid:string):Observable<any> {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/notifications/v2/{uuid}";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "uuid": uuid
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "GET",
          queryUrl: queryUrl,
          headers: headers
      };
      return this.HttpClient.executeRequest(config);
    }

    updateNotification(body:Object, uuid:string):Observable<any> {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/notifications/v2/{uuid}";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "uuid": uuid
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "content-type": "application/json; charset=utf-8",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      this.APIHelper.cleanObject(body);

      let config = {
          method: "POST",
          queryUrl: queryUrl,
          headers: headers,
          body: body
      };
      return this.HttpClient.executeRequest(config);
    }

    deleteNotification(uuid:string):Observable<any> {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/notifications/v2/{uuid}";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "uuid": uuid
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "DELETE",
          queryUrl: queryUrl,
          headers: headers,
      };
      return this.HttpClient.executeRequest(config);
    }


    fireNotification(body:Object, uuid:string):Observable<any> {
      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/notifications/v2/{uuid}/attempts";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "uuid": uuid
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "content-type": "application/json; charset=utf-8",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      this.APIHelper.cleanObject(body);

      let config = {
          method: "POST",
          queryUrl: queryUrl,
          headers: headers,
          body: body
      };
      return this.HttpClient.executeRequest(config);
    }

}
