/* tslint:disable:no-unused-variable */

import { HttpModule, Http, BaseRequestOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { TestBed, async, inject } from '@angular/core/testing';
import { APIHelper } from '../http/http.apihelper';
import { Configuration } from '../http/http.configuration';
import { HttpClient } from '../http/http.client';
import { MonitorsService } from './monitors.service';

describe('MonitorsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        APIHelper,
        Configuration,
        HttpClient,
        {
          provide: Http,
          useFactory: (mockBackend, options) => {
            return new Http(mockBackend, options);
          },
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
        MonitorsService
      ]
    });
  });

  it('should ...', inject([MonitorsService], (service: MonitorsService) => {
    expect(service).toBeTruthy();
  }));
});
