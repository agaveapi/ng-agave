import { Injectable } from '@angular/core';
import { APIHelper } from '../http/http.apihelper';
import { Configuration } from '../http/http.configuration';
import { HttpClient } from '../http/http.client';
import { Observable } from 'rxjs/Rx';

import { Http, Response } from '@angular/http';

@Injectable()
export class FilesService{
  constructor(private HttpClient:HttpClient, private Configuration:Configuration, private APIHelper:APIHelper){};

  uploadBlob(content:string, fileName:string, path:string, append:string, fileType:string, notifications:string) {
    append = append || "false";

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let formDataDictionary = {
        "fileType": fileType,
        "notifications": notifications,
        "append": (null != append) ? append : "false"
    };

    this.APIHelper.cleanObject(formDataDictionary);

    let boundary = "---------------------------7da24f2e50046";

    let body = '--' + boundary + '\r\n'
        + 'Content-Disposition: form-data; name="fileToUpload";'
        + 'filename="' + fileName + '"\r\n'
        + 'Content-type: application/octet-stream\r\n\r\n'
        + content + '\r\n';

    for(let key in formDataDictionary) {
        body += '--' + boundary + '\n'
            + 'Content-Disposition: form-data; name="'+key+'";'
            + 'Content-type: application/octet-stream\r\n\r\n'
            + (<any>formDataDictionary)[key] + '\r\n';
    }

    body += '--'+ boundary + '--'
        + '\r\n';

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        formData: body,
        cache: true
    };
    return this.HttpClient.executeRequest(config);
  }

  uploadFileItemToDefaultSystem(fileToUpload:string, append:string, fileName:string, fileType:string, notifications:string, path:string) {
    append = append || "false";

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let formDataDictionary = {
        "fileToUpload": fileToUpload,
        "fileName": fileName,
        "fileType": fileType,
        "notifications": notifications,
        "append": (null != append) ? append : "false"
    };

    this.APIHelper.cleanObject(formDataDictionary);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        formData: formDataDictionary,
    };
    return this.HttpClient.executeRequest(config);
  }

  updateInvokeFileActionOnDefaultSystem(body:Object, path:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "PUT",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  deleteFileItemOnDefaultSystem(path:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "naked": true
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  importFileItem(urlToIngest:string, path:string, systemId:string, fileType:string, notifications:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/system/{systemId}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path,
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "multipart/form-data",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let formDataDictionary = {
        "urlToIngest": urlToIngest,
        "fileName": path,
        "fileType": fileType,
        "notifications": notifications
    };

    this.APIHelper.cleanObject(formDataDictionary);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        formData: formDataDictionary,
    };
    return this.HttpClient.executeRequest(config);
  }

  uploadFileItem(fileToUpload:string, path:string, systemId:string, fileName:string, fileType:string, notifications:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/system/{systemId}/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path,
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "multipart/form-data",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let formDataDictionary = {
        "fileToUpload": fileToUpload,
        "fileName": fileName,
        "fileType": fileType,
        "notifications": notifications
    };

    this.APIHelper.cleanObject(formDataDictionary);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        formData: formDataDictionary,
    };
    return this.HttpClient.executeRequest(config);
  }

  updateInvokeFileItemAction(body:Object, systemId:string, path:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/system/{systemId}/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId,
        "path": path
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "PUT",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  listFileItems(systemId:string, path:string, limit:number, offset:number) {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/listings/system/{systemId}/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId,
        "path": path
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
        cache: false
    };
    return this.HttpClient.executeRequest(config);
  }

  updateFileItemPermissionsOnDefaultSystem(body:Object, path:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/pems/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  updateFileItemPermission(body:Object, systemId:string, path:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/pems/system/{systemId}/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId,
        "path": path
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  listFileItemHistory(filePath:string, systemId:string, created:string, limit:number, offset:number, status:string) {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/history/system/{systemId}/{filePath}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "filePath": filePath,
        "systemId": systemId
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "created": created,
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0,
        "status": status
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  listFileItemHistoryOnDefaultSystem(created:string, limit:number, offset:number, path:string, status:string) {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/history/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "created": created,
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0,
        "status": status
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteFileItem(path:string, systemId:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/system/{systemId}/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path,
        "systemId": systemId
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  deleteClearFileItemPermissions(systemId:string, path:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/pems/system/{systemId}/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId,
        "path": path
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  getDownloadFileItem(path:string, systemId:string, force:boolean) {
    force = force || false;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/system/{systemId}/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path,
        "systemId": systemId
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "force": (null != force) ? force : false
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
        cache: false
    };
    return this.HttpClient.executeRequest(config);
  }

  getDownloadFileItemOnDefaultSystem(path:string, force:boolean) {
    force = force || false;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "force": (null != force) ? force : false
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  listFileItemsOnDefaultSystem(limit:number, offset:number, path:string) {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/listings/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  listFileItemPermissions(systemId:string, limit:number, offset:number, path:string) {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/pems/system/{systemId}/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId,
        "path": path
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  listFileItemPermissionsOnDefaultSystem(limit:number, offset:number, path:string) {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/pems/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  createImportFileItem(body:Object, systemId:string, path:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/system/{systemId}/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "systemId": systemId,
        "path": path
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }

  createImportFileItemToDefaultSystem(body:Object, path:string) {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/files/v2/media/{path}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "path": path
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Accept": "application/json",
        "Content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };
    return this.HttpClient.executeRequest(config);
  }
}
