import { Injectable } from '@angular/core';
import { APIHelper } from '../http/http.apihelper';
import { Configuration } from '../http/http.configuration';
import { HttpClient } from '../http/http.client';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class TenantsService{

  constructor(private HttpClient:HttpClient, private Configuration:Configuration, private APIHelper:APIHelper){};

  listTenants(limit:number, offset:number, queryParameters:string):Observable<any> {
    limit = limit || 100;
    offset = offset || 0;
    queryParameters = queryParameters || null;

    let baseUri = "https://agaveapi.co"
    let queryBuilder = baseUri + "/tenants/";

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, queryParameters)

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json"
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
        cache: true
    };
    return this.HttpClient.executeRequest(config);
  }

}
