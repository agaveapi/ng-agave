import { Injectable } from '@angular/core';
import { APIHelper } from '../http/http.apihelper';
import { Configuration } from '../http/http.configuration';
import { HttpClient } from '../http/http.client';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class MetaService {

  constructor(private HttpClient:HttpClient, private Configuration:Configuration, private APIHelper:APIHelper){};

  listMetadata(q:string, limit:number, offset:number):Observable<any> {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/data";

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "q": q,
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
        cache: false
    };

    return this.HttpClient.executeRequest(config);
  }

  addMetadata(body:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/data";

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  getMetadata(uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/data/{uuid}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  updateMetadata(body:Object, uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/data/{uuid}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteMetadata(uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/data/{uuid}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  addMetadataSchema(body:Object):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas";

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  updateMetadataSchema(body:Object, uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas/{uuid}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteMetadataSchema(uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas/{uuid}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  listMetadataPermissions(uuid:string, limit:number, offset:number):Observable<any> {
      limit = limit || 100;
      offset = offset || 0;

      let baseUri = this.Configuration.getBaseURI();
      let queryBuilder = baseUri + "/meta/v2/data/{uuid}/pems";

      queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
          "uuid": uuid
      });

      queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
          "limit": (null != limit) ? limit : 100,
          "offset": (null != offset) ? offset : 0
      });

      let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

      let headers = {
          "accept": "application/json",
          "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
      };

      let config = {
          method: "GET",
          queryUrl: queryUrl,
          headers: headers,
      };

      return this.HttpClient.executeRequest(config);
  }

  addMetadataPermission(body:Object, uuid:number):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/data/{uuid}/pems";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteClearMetadataPermissions(uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/data/{uuid}/pems";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  getMetadataPermission(username:string, uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/data/{uuid}/pems/{username}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "username": username,
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  updateMetadataPermission(body:Object, username:string, uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/data/{uuid}/pems/{username}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "username": username,
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteMetadataPermission(username:string, uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/data/{uuid}/pems/{username}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "username": username,
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  listMetadataSchemaPermissions(uuid:string, limit:number, offset:number):Observable<any> {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas/{uuid}/pems";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  addMetadataSchemaPermission(body:Object, uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas/{uuid}/pems";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteClearMetadataSchemaPermissions(uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas/{uuid}/pems";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  getMetadataSchemaPermission(username:string, uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas/{uuid}/pems/{username}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "username": username,
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };
    return this.HttpClient.executeRequest(config);
  }

  updateMetadataSchemaPermission(body:Object, username:string, uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas/{uuid}/pems/{username}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "username": username,
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "content-type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    this.APIHelper.cleanObject(body);

    let config = {
        method: "POST",
        queryUrl: queryUrl,
        headers: headers,
        body: body
    };

    return this.HttpClient.executeRequest(config);
  }

  deleteSchemaPermission(username:string, uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas/{uuid}/pems/{username}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "username": username,
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "DELETE",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  getMetadataSchema(uuid:string):Observable<any> {
    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas/{uuid}";

    queryBuilder = this.APIHelper.appendUrlWithTemplateParameters(queryBuilder, {
        "uuid": uuid
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }

  listMetadataSchema(q:string, limit:number, offset:number):Observable<any> {
    limit = limit || 100;
    offset = offset || 0;

    let baseUri = this.Configuration.getBaseURI();
    let queryBuilder = baseUri + "/meta/v2/schemas";

    queryBuilder = this.APIHelper.appendUrlWithQueryParameters(queryBuilder, {
        "q": q,
        "limit": (null != limit) ? limit : 100,
        "offset": (null != offset) ? offset : 0
    });

    let queryUrl = this.APIHelper.cleanUrl(queryBuilder);

    let headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + this.Configuration.getOAuthAccessToken()
    };

    let config = {
        method: "GET",
        queryUrl: queryUrl,
        headers: headers,
    };

    return this.HttpClient.executeRequest(config);
  }
}
