import { NgAgavePage } from './app.po';

describe('ng-agave App', function() {
  let page: NgAgavePage;

  beforeEach(() => {
    page = new NgAgavePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
